terraform {
  required_providers {
    juju = {
      version = "~> 0.10.0"
      source  = "juju/juju"
    }
  }
}

provider "juju" {}

resource "juju_model" "cos" {
  name = "cos"

  cloud {
    name   = "microk8s-serverstack"
    region = "localhost"
  }
}

resource "juju_application" "ca" {
  name = "ca"

  model = juju_model.cos.name

  charm {
    name = "self-signed-certificates"
    channel  = "latest/stable"
  }
}


