resource "juju_application" "alertmanager" {
  name = "alertmanager"

  model = juju_model.cos.name

  trust = true

  charm {
    name = "alertmanager-k8s"
    channel  = "latest/stable"
  }
}


